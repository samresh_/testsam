FROM tomcat:latest
ADD target/Spring3HibernateApp.war /usr/local/tomcat/webapps/
RUN sed -i 's/port="8080"/port="9090"/' ${CATALINA_HOME}/conf/server.xml
EXPOSE 9090
CMD ["catalina.sh", "run"]
